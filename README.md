lamp-centos6
================

[![Gitter](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/matchy2/lamp-centos6?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

CentOS-6 (i386) の LAMP 環境作成用テンプレート

開発環境の想定
---------------

* DHCP が稼働している LAN。
* 複数のメンバーがそれぞれ自分のマシンに自分のユーザー名でログインしており、ユーザー名はコンフリクトしない。
* DHCP が稼働してる LAN でない場合は、Vagrantfile の `config.vm.network "public_network"` をコメントアウトし、`config.vm.network "forwarded_port", guest: 80, host: 8080` を有効にするなどすること。また、その場合には下記で説明する Bonjour / Avahi による名前解決は不能である。

必要なもの
----------

* [Oracle VM VirtualBox](http://www.oracle.com/technetwork/jp/server-storage/virtualbox/overview/)
* [Vagrant](http://www.vagrantup.com/)
* [Bonjour Print Services for Windows](http://support.apple.com/kb/DL999?viewlocale=ja_JP) (Windows の場合)
* UNIX 及び Windows の改行コードと UTF-8 が使えるテキストエディタ
* 黒い画面に負けない勇気


入れとくと便利なVagrantプラグイン
----------------------------------
* vagrant-cachier (yum をキャッシュして２つ目以降の VM の設定をはやくする)
* vagrant-vbguest (VirtualBox Guest Additions の自動アップデート)
* sahara (VM を Sandbox 化し、rollback などを可能にする)

コマンドラインから以下のようにインストールする

````
vagrant plugin install vagrant-cachier
````

開始手順
--------

本プロジェクトを clone する。もしくは画面右下の「Download Zip」でダウンロードし、解凍する。

```
git clone https://github.com/matchy2/lamp-centos6.git
cd lamp-centos6
```

Vagrantfile 内の chef.json と、chef-repo/environments/develop.json ファイルを適切に修正する。以下修正ポイント。

まずは Vagrantfile。

```ruby:Vagrantfile
chef.json = {
  'host' => {
     # ホスト名。後半はログインユーザー名を環境変数から取得。
     # 通常は変更の必要なし
    'name' => 'vm-' + (ENV['USERNAME'] || ENV['USER'])
  }
````

そして chef-repo/environments/develop.json。

````javascript:develop.json
  'postfix': { //  OBT25 対策。不要であればまるごとコメントアウト
    'relay': {
      'smtphost'  : 'smtp.gmail.com', // メール送信サーバー名
      'smtpport'  : '587',            // メール送信サーバーポート番号
      'usetls'    : 'yes',            // メール送信にSSL/TLSを使うならyes
      'smtpuser'  : 'foo@gmail.com',  // メール送信サーバーログイン名
      'smtppasswd': 'your password'   // メール送信サーバーパスワード
    }
  },
  'mysql': {
    'rootpasswd': 'rootpasswd', // MySQL の root (管理者) パスワード
    'mydbname'  : 'mydb',       // MySQL で作成したいデータベース名
    'myuser'    : 'myuser',     // MySQL で作成したいデータの接続ユーザー名
    'mypasswd'  : 'mypass'      // MySQL で作成したいデータの接続パスワード
  }
````

Vagrant を起動し、プロンプトが表示されるまでひたすら待つ。

```
vagrant up
```

開発環境
--------

### Web 公開ディレクトリ

上記 Vagrantfile のあるディレクトリ下に自動的に作成された `www/htdocs` が apache の DocumentRoot に設定されているので、ホスト OS 側のエディタ、IDE、オーサリングツールなどでファイルを編集する。

### MySQL

上記 Vagrantfile で設定したデータベース及び接続ユーザーが作成されている。

### Web アクセス

Vagrantfile の `[host][name]` を変更していなければ、`http://vm-ホストへのログインユーザー名.local/` で Bonjour / Avahi 名前解決し、アクセス可能である。  

### PhpMyAdmin

MySQL の操作をする場合は、`http://vm-ホストへのログインユーザー名.local/phpmyadmin/` にアクセスすると PhpMyAdmin を使用可能。特にアクセス制限はかけていないので注意すること。

ゲストマシンの終了
------------------

作業終了時、ホストマシンを終了する前に以下のコマンドでゲストを終了させる。

    vagrant halt

次回起動させるときは上記同様 `vagrant up`。初回ほどは時間はかからない。

開発終了と再開
----------------

開発が終了しディスク容量を節約したい場合は、まずデータベースのバックアップをとる。

sh ディレクトリ内の mysqlbackup.sh / mysqlrestore.sh の、下記の部分を必要であれば適切に変更する。

```` bash
ROOTPASSWD="rootpasswd" # Vagrantfile で指定した MySQL の root パスワード
DBNAME="mydb"           # Vagrantfile で指定した作成したデータベース名
````

Windows の場合は以下のバッチコマンドを実行。ただし、MSYS や cygwin でコマンドラインの ssh が使えるようにしておく必要がある。

    windows\mysqlbackup.cmd

そして以下のコマンドを実行し、仮想マシンを削除する。

    vagrant destroy -f

開発再開時には `vagrant up` すると改めて最初から環境構築が行われる。  
Windows の場合は以下のバッチコマンドでバックアップした DB をリストアする。

    windows\mysqlrestore.cmd

Mac OS その他をお使いの方は UNIX コマンドが使えると思うので、sh ディレクトリ内 mysqlbackup.sh / mysqlrestore.sh を参考にされたい。


本番環境の構築
----------------

knife-zero を使って本プロジェクトに含まれるレシピを使用することで、上記開発環境とほぼ同じ設定を本番環境に作ることができる。  

必要なもの
-----------

上記開発環境に必要なものプラス knife-zero を使えるようにする。Windows だと結構大変なので、Linux か Mac でやるのがオススメ。

Environments ファイルの編集
-------------------

chef-repos/environments/production.json ファイルを適切に修正する。以下修正ポイント。


````javascript:production.json
  "host": {
    // サーバーの外部公開用 FQDN
    "fqdn": "foo.example.net"
  },
  "postfix" : { // OBT25 対策。不要であればまるごとコメントアウト
    "relay" : {
      "smtphost"   : "smtp.gmail.com", // SMTPホスト名
      "smtpport"   : "587",            // SMTP ポート番号
      "usetls"     : "yes",            // SSL/TLSを使うならyes
      "smtpuser"   : "foo@gmail.com",  // SMTP ログイン名
      "smtppasswd" : "your password"   // SMTP パスワード
    }
  },
  "mysql" : {
    "rootpasswd" : "rootpasswd", // MySQL の root パスワード
    "mydbname"   : "mydb",       // MySQL で作成したいデータベース名
    "myuser"     : "myuser",     // MySQL で作成したいデータの接続ユーザー名
    "mypasswd"   : "mypass"      // MySQL で作成したいデータの接続パスワード
  }
````

本番環境の設定
---------------

* 一般ユーザーで ssh ログイン可能にする
* ssh 公開鍵をセットし、クライアントは ssh-agent を使用して、パスワードなしでログインできるようにする
* そのユーザーは sudo でパスワードなしでなんでもできるできるようにしとく
* root アカウントおよびパスワード認証は禁止するので注意
* sudoers の「Defaults requiretty」はコメントアウトしておく


Chef 化
--------

クライアント側で実行

````bash
knife zero bootstrap 本番サーバーのFQDN -x ログインユーザー名 --sudo
````

対象マシンに Ruby 実行環境コミで Chef の rpm がインストールされる。

続けて以下を実行

````bash
knife node list
````

例えば以下のように表示される。

````
productionhost
zerosanmple
````
このうち「zerosample」以外のもの、この例では「productionhost」が先ほど初期化した本番サーバー。

Nodeファイル編集
----
chef-erpo/nodes/zerosample.json を参考に、上の例で言うところの chef-repo/nodes/productionhost.json の適切な位置に以下を書き加える。「name:～」の行の次ぐらいで。

````
    "chef_environment": "production",
    "run_list": [
      "role[production]"
    ]
````

実行
----
ここでパスワード認証とrootログインができなくなるので、一般ユーザーで公開鍵認証でsshログインできることを再度確認する。

問題なければ以下を実行。「productionhost」は上記のように環境によって異なる。

````bash
knife zero chef_client 'name:producatinohost' --attribute ipaddress -x ログインユーザー名 --sudo
````

開発環境との違い
-----------------
* Avahi はインストールしない
* OS のホスト名は設定しない
* Apache/Postfix のホスト名指定は正式な FQDN
* iptables のファイアーウォールはより厳格に
* PhpMyAdmin もインストールしない
* DenyHosts という ssh への連続アクセスを締め出すヤツをインストールしている
* ssh は root ログイン禁止・パスワードログイン禁止
