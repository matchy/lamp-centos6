#
# Cookbook Name:: php
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

%w{php php-pear php-gd php-mbstring php-xml php-mcrypt php-mysql}.each do |p|
  package p do
    action :install
  end
end

%w{noexpose errors}.each do |t|
  template "#{t}.ini" do
    path "/etc/php.d/#{t}.ini"
    source "#{t}.ini.erb"
    mode 0644
    notifies :restart, 'service[httpd]'
  end
end
