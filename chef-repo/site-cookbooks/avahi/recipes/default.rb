#
# Cookbook Name:: avahi
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

r=package 'dbus' do
  action :install
end
r.run_action(:install)
r=service 'messagebus' do
  action [:start, :enable]
end
r.run_action(:start)


%w{avahi nss-mdns}.each do |p|
  package p do
    action :install
  end
end

file '/etc/sysconfig/network' do
  unless File.exists?('/etc/hostname') then
    f = Chef::Util::FileEdit.new(path)
    f.search_file_replace_line(%r{^HOSTNAME=},
                               "HOSTNAME=#{node['host']['name']}\n")
    f.write_file

    notifies :run, 'execute[sethostname]', :immediately
    notifies :restart, 'service[avahi-daemon]'
  end
end
file '/etc/hostname' do
  if File.exists?('/etc/hostname') then
    f = Chef::Util::FileEdit.new(path)
    f.search_file_replace_line(%r{^.*$},
                               "#{node['host']['name']}\n")
    f.write_file

    notifies :run, 'execute[sethostname]', :immediately
    notifies :restart, 'service[avahi-daemon]'
  end
end

execute 'sethostname' do
  command "hostname #{node['host']['name']}"
  action :run 
end

service 'avahi-daemon' do
  action [ :enable, :start, :restart ]
end

