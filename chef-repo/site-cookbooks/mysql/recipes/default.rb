#
# Cookbook Name:: mysql
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

%w{mysql mysql-server}.each do |p|
  package p do
    action :install
  end
end

template 'my.cnf' do
  path '/etc/my.cnf'
  source 'my.cnf.erb'
  mode 0644
  notifies :restart, 'service[mysqld]'
end

service 'mysqld' do
  action [:start, :enable]
end

bash 'mysql_secure_install emulate' do
  code <<-"EOH"
    rootpass="#{node['mysql']['rootpasswd']}"
    mysql -e "drop database test;" -Dmysql
    mysql -e "delete from db where db like 'test%';" -Dmysql
    mysql -e "delete from user where user='';" -Dmysql
    mysql -e "delete from user where user='root' and host=\'#{node[:hostname]}\';" -Dmysql
    mysql -e "update user set password=PASSWORD(\'${rootpass}\') where user='root';" -Dmysql
    mysql -e "flush privileges;" -Dmysql
  EOH
  action :run
  only_if "mysql -u root -e 'show databases;'"
end

bash 'create db' do
  code <<-"EOH"
    rootpass="#{node['mysql']['rootpasswd']}"
    mydb="#{node['mysql']['mydbname']}"
    myuser="#{node['mysql']['myuser']}"
    mypass="#{node['mysql']['mypasswd']}"
    mysql -e "create database ${mydb} default character set utf8;" -Dmysql -p${rootpass}
    mysql -e "grant all privileges on ${mydb}.* to ${myuser}@localhost identified by \'${mypass}\';" -Dmysql -p${rootpass}
    mysql -e "grant all privileges on ${mydb}.* to ${myuser}@'127.0.0.1' identified by \'${mypass}\';" -Dmysql -p${rootpass}
  EOH
  action :run
  not_if { Dir.exists?("/var/lib/mysql/#{node['mysql']['mydbname']}") }
end
