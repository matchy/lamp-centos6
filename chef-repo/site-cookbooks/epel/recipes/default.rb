#
# Cookbook Name:: epel
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

r=bash 'epel' do
  user 'root'
  if node[:platform_family].start_with?('6') then
    code <<-EOH
    yum install -y --nogpgcheck http://dl.fedoraproject.org/pub/epel/6/i386/epel-release-6-8.noarch.rpm
    rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-6
    EOH
  else
    code <<-EOH
    yum install -y epel-release
    EOH
  end 
  not_if { File.exists?('/etc/yum.repos.d/epel.repo') }
end
r.run_action(:run)
