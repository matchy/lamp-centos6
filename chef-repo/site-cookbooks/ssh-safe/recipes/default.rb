#
# Cookbook Name:: webroot
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

file '/etc/ssh/sshd_config' do
  f = Chef::Util::FileEdit.new(path)
  f.search_file_replace_line(%r{^#?PermitRootLogin},
                             "PermitRootLogin no\n")
  f.search_file_replace_line(%r{^#?PermitEmptyPasswords},
                             "PermitEmptyPasswords no\n")
  f.search_file_replace_line(%r{^PasswordAuthentication yes},
                             "PasswordAuthentication no\n")
  f.write_file
  notifies :restart, 'service[sshd]'
end

service 'sshd' do
  action [:start, :enable]
end
