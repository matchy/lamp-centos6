#
# Cookbook Name:: iptables
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

package 'lokkit' do
  action :install
end
bash 'lokkit' do
  user 'root'

  if node['host']['mode'] == 'desktop' then
    code <<-EOH
      lokkit --selinux=disabled --default #{node['host']['mode']} -p 22:tcp -p 80:tcp
    EOH
  else
    code <<-EOH
      lokkit --selinux=disabled --default #{node['host']['mode']} -p 80:tcp
    EOH
  end
end
service 'iptables' do
  action [:start, :enable]
end
