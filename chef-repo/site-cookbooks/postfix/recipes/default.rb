#
# Cookbook Name:: postfix
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

service 'sendmail' do
  action [:stop, :disable]
end
package 'sendmail' do
  action :remove
end

%w{postfix cyrus-sasl-plain cyrus-sasl-md5}.each do |p|
  package p do
    action :install
  end
end

file '/etc/postfix/main.cf' do
  hostname = node['host']['fqdn'] || node['host']['name']
  if md = hostname.match(/[^.]+\.(.+)/) then
    domainname = md[1]
  else
    domainname = 'local'
    hostname += ".#{domainname}"
  end

  f = Chef::Util::FileEdit.new(path)

  # basic
  f.search_file_replace(%r{#myhostname = virtual\.domain\.tld},
                        "myhostname = #{hostname}")
  f.search_file_replace(%r{#mydomain = domain\.tld},
                        "mydomain = #{domainname}")
  f.search_file_replace(%r{#myorigin = \$myhostname},
                        'myorigin = $myhostname')
  f.write_file

  # security
  f.search_file_replace(%r{#mynetworks = .+, 127\.0\.0\.0/8},
                        "mynetworks = #{node[:ipaddress]}/32, 127.0.0.0/8")
  f.search_file_replace(%r{#smtpd_banner = \$myhostname ESMTP \$mail_name$},
                        'smtpd_banner = $myhostname ESMTP')
  f.write_file

  f.insert_line_if_no_match(%r{allow_percent_hack *= *yes},
                            "allow_percent_hack = yes\n")
  f.write_file
  f.insert_line_if_no_match(%r{swap_bangpath *= *yes},
                            "swap_bangpath = yes\n")
  f.write_file
  f.insert_line_if_no_match(%r{disable_vrfy_command *= *yes},
                            "disable_vrfy_command = yes\n")
  f.write_file

  # lifetime
  f.insert_line_if_no_match(%r{queue_run_delay},
                            "queue_run_delay = 100s\n")
  f.write_file
  f.insert_line_if_no_match(%r{minimal_backoff_time},
                            "minimal_backoff_time = 100s\n")
  f.write_file
  f.insert_line_if_no_match(%r{maximal_backoff_time},
                            "maximal_backoff_time = 600s\n")
  f.write_file
  f.insert_line_if_no_match(%r{maximal_queue_lifetime},
                            "maximal_queue_lifetime = 24h\n")
  f.write_file
  f.insert_line_if_no_match(%r{bounce_queue_lifetime},
                            "bounce_queue_lifetime = 24h\n")
  f.write_file

  # smtp auth
  if node['postfix'] && node['postfix']['relay'] &&
      node['postfix']['relay']['smtphost'] then
    relayhost = node['postfix']['relay']
    smtphost = "[#{relayhost['smtphost']}]"
    smtphost += ":#{relayhost['smtpport']}" if relayhost['smtpport']
    username = relayhost['smtpuser']
    passwd   = relayhost['smtppasswd']

    if username && passwd then
      f.insert_line_if_no_match(%r{smtp_sasl_auth_enable *= *yes},
                                "smtp_sasl_auth_enable = yes\n")
      f.write_file
      f.insert_line_if_no_match(%r{smtp_sasl_mechanism_filter},
                                "smtp_sasl_mechanism_filter = plain, login, cram-md5, digest-md5\n")
      f.write_file
      f.insert_line_if_no_match(%r{smtp_sasl_password_maps},
                                "smtp_sasl_password_maps = hash:/etc/postfix/sasl_passwd\n")
      f.write_file
    end
    if relayhost['usetls'] then
      f.insert_line_if_no_match(%r{smtp_use_tls *= *yes},
                                "smtp_use_tls = yes\n")
      f.write_file
      f.insert_line_if_no_match(%r{smtp_sasl_tls_security_options *= *noanonymous},
                                "smtp_sasl_tls_security_options = noanonymous\n")
      f.write_file
      f.insert_line_if_no_match(%r{smtp_tls_CApath},
                                "smtp_tls_CApath = /etc/pki/tls/certs/ca-bundle.crt\n")
      f.write_file
    end
    f.insert_line_if_no_match(%r{^relayhost *=},
                              "relayhost=#{smtphost}\n")
    f.write_file
  end

  f.write_file
  notifies :restart, 'service[postfix]'
end

file '/etc/postfix/sasl_passwd' do
  File.open(path, 'a', 0600).close unless File.exists? path
  f = Chef::Util::FileEdit.new(path)

  if node['postfix'] && node['postfix']['relay'] &&
      node['postfix']['relay']['smtphost'] then
    relayhost = node['postfix']['relay']
    smtphost = "[#{relayhost['smtphost']}]"
    smtphost += ":#{relayhost['smtpport']}" if relayhost['smtpport']
    username = relayhost['smtpuser']
    passwd   = relayhost['smtppasswd']

    f.insert_line_if_no_match(%r{#{Regexp.escape(smtphost)}},
                              "#{smtphost} #{username}:#{passwd}\n")
  end

  f.write_file
  notifies :run, 'execute[passwd_makemap]'
end

execute 'passwd_makemap' do
  command "postmap /etc/postfix/sasl_passwd"
  action :nothing
end

service 'postfix' do
  action [:start, :enable]
end
