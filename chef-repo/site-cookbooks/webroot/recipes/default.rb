#
# Cookbook Name:: webroot
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

directory '/vagrant/www/htdocs' do
  recursive true
  action :create
end

template 'docroot.conf' do
  if Dir.exists?("/etc/httpd/conf.modules.d") then
    path '/etc/httpd/conf.d/docroot.conf'
  else
    path '/etc/httpd/conf.mysite.d/docroot.conf'
  end
  source 'docroot.conf.erb'
  mode 0644
  notifies :restart, 'service[httpd]'
end
