#
# Cookbook Name:: webroot
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

r=package 'denyhosts' do
  action :install
end
r.run_action(:install)

file '/etc/hosts.evil' do
  action :create
end

file '/etc/denyhosts.conf' do
  f = Chef::Util::FileEdit.new(path)
  f.search_file_replace(%r{^HOSTS_DENY = /etc/hosts.deny},
                        '#HOSTS_DENY = /etc/hosts.deny')
  f.search_file_replace(%r{#HOSTS_DENY = /etc/hosts.evil},
                        'HOSTS_DENY = /etc/hosts.evil')
  f.search_file_replace(%r{^BLOCK_SERVICE  = sshd},
                        '#BLOCK_SERVICE  = sshd')
  f.search_file_replace(%r{#BLOCK_SERVICE = *$},
                        'BLOCK_SERVICE =')
  f.search_file_replace(%r{#?HOSTNAME_LOOKUP=YES},
                        'HOSTNAME_LOOKUP=NO')
  f.search_file_replace(%r{^#SYNC_SERVER =},
                        'SYNC_SERVER =')
  f.search_file_replace(%r{^SYNC_DOWNLOAD = no},
                        '#SYNC_DOWNLOAD = no')
  f.search_file_replace(%r{#SYNC_DOWNLOAD = yes},
                        'SYNC_DOWNLOAD = yes')
  f.write_file
  notifies :restart, 'service[denyhosts]'
end

file '/etc/hosts.deny' do
  f = Chef::Util::FileEdit.new(path)
  f.insert_line_if_no_match(%r{ALL *: *ALL},
                            "ALL: ALL\n")
  f.write_file
end
file '/etc/hosts.allow' do
  f = Chef::Util::FileEdit.new(path)
  f.insert_line_if_no_match(%r{ALL *: *ALL EXCEPT /etc/hosts.evil},
                            "ALL: ALL EXCEPT /etc/hosts/evil\n")
  f.write_file
end

service 'denyhosts' do
  action [:start, :enable]
end
