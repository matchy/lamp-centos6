#
# Cookbook Name:: phpmyadmin
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

r=package 'phpMyAdmin' do
  action :install
end
r.run_action(:install)


file '/etc/httpd/conf.d/phpMyAdmin.conf' do
  f = Chef::Util::FileEdit.new(path)
  f.search_file_replace(%r{<RequireAny>},
                        '#<RequireAny>')
  f.search_file_replace(%r{</RequireAny>},
                        '#</RequireAny>')
  f.search_file_replace(%r{Require ip 127\.0\.0\.1},
                        'Require all granted')
  f.search_file_replace(%r{Require ip ::1},
                        '#Require ip ::1')
  f.search_file_replace(%r{Deny from All},
                        '#Deny from All')
  f.search_file_replace(%r{Allow from 127\.0\.0\.1},
                        '#Allow from 127.0.0.1')
  f.search_file_replace(%r{Allow from ::1},
                        '#Allow from ::1')
  f.write_file
  notifies :restart, 'service[httpd]'
end
