#
# Cookbook Name:: apache
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

r=package 'httpd' do
  action :install
end
r.run_action(:install)

directory '/etc/httpd/conf.mysite.d/' do
  owner 'root'
  group 'root'
  recursive true
  mode 0755
  action :create
  not_if { Dir.exists?("/etc/httpd/conf.modules.d") }
end

%w{defaults tuning}.each do |t|
  template "apache-#{t}" do
    if Dir.exists?("/etc/httpd/conf.modules.d") then
      path "/etc/httpd/conf.d/000#{t}.conf"
    else
      path "/etc/httpd/conf.mysite.d/000#{t}.conf"
    end
    source "#{t}.conf.erb"
    mode 0644
    notifies :restart, 'service[httpd]'
  end
end

file '/etc/httpd/conf/httpd.conf' do
  unless Dir.exists?("/etc/httpd/conf.modules.d") then
    f = Chef::Util::FileEdit.new(path)
    f.insert_line_if_no_match(%r{Include conf\.mysite\.d/\*\.conf},
                              "Include conf.mysite.d/*.conf\n")
    f.write_file
  end
  notifies :restart, 'service[httpd]'
end

service 'httpd' do
  action [:start, :enable]
end
