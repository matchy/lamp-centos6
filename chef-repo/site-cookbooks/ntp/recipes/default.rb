#
# Cookbook Name:: webroot
# Recipe:: default
#
# Copyright 2014, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

r=package 'ntp' do
  action :install
end
r.run_action(:install)

file '/etc/ntp.conf' do
  f = Chef::Util::FileEdit.new(path)

  f.insert_line_after_match(%r{server 0\.centos\.pool\.ntp\.org},
                            "server ntp.jst.mfeed.ad.jp iburst\n" +
                            "server ntp.nict.jp iburst\n" +
                            "server ats1.e-timing.ne.jp iburst\n")
  f.write_file

  f.search_file_delete_line(%r{server [0-9]\.centos\.pool\.ntp\.org})
  f.write_file

  notifies :restart, 'service[ntpd]'
end

service 'ntpd' do
  action [:start, :enable]
end
