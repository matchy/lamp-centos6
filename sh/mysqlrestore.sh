#!/bin/sh

ROOTPASSWD="rootpasswd"
DBNAME="mydb"
DIRNAME="mysqlbackup"
FILENAME="$DBNAME.sql"
BACKUPFILE="/vagrant/$DIRNAME/$FILENAME"

if [ -f $BACKUPFILE ]; then
  mysql -uroot -p$ROOTPASSWD $DBNAME < $BACKUPFILE
else
  echo "backup file is not found"
fi
