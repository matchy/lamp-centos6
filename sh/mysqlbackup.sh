#!/bin/sh

ROOTPASSWD="rootpasswd"
DBNAME="mydb"
DIRNAME="mysqlbackup"
FILENAME="$DBNAME.sql"
BACKUPDIR="/vagrant/$DIRNAME"

if [ ! -d $BACKUPDIR ]; then
  mkdir $BACKUPDIR
fi

mysqldump -uroot -p$ROOTPASSWD -x $DBNAME > $BACKUPDIR/$FILENAME
